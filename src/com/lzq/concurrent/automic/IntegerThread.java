package com.lzq.concurrent.automic;

public class IntegerThread extends Thread{

    Integer integer;

    public IntegerThread(Integer integer) {
        this.integer = integer;
    }

    @Override
    public void run() {
        integer++;
    }
}
