package com.lzq.concurrent.semaphore;

import java.util.concurrent.Semaphore;

/**
 * semaphore 信号量 设置一个容量
 *      同一时间内只能有多少个线程在运行
 *      一个线程要拿走多少资源（容量）
 *      可控制并发数
 */

public class Main {
    public static void main(String[] args) {
        Semaphore semaphore = new Semaphore(2);

        for (int i = 0; i < 5; i++) {
            new MyThread("学生-" + (i + 1), semaphore).start();
        }

    }
}
