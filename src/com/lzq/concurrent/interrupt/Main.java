package com.lzq.concurrent.interrupt;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        MyThread myThread = new MyThread();
        myThread.start();
        Thread.sleep(10);
        // 中断线程
        myThread.interrupt();
        Thread.sleep(100);
        System.out.println("===");
        System.exit(0);
    }
}