package com.lzq.lambda;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;

public class Lambda1 {

    public static void main(String[] args) {
        List<Person> list = new ArrayList<>();

        list.add(new Person("小明", 12));
        list.add(new Person("小红", 10));
        list.add(new Person("小华", 18));
        list.add(new Person("小金", 20));
        list.add(new Person("小小", 6));

        System.out.println(list);
        System.out.println("--------------------排序前后------------------");

        Comparator<Person> comparator = new Comparator<Person>() {
            @Override
            public int compare(Person o1, Person o2) {
                return o1.getAge() - o2.getAge(); // 中间写了这么一个匿名内部类,实际上只有这句话有用, 那怎么简化呢?
            }
        };

        list.sort(comparator);
        System.out.println(list);

        // lambda 简化
        Comparator<Person> comparator1 = (x, y) -> Integer.compare(x.getAge(), y.getAge());
        list.sort(comparator1);



    }

}
