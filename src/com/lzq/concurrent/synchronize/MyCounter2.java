package com.lzq.concurrent.synchronize;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class MyCounter2 {
    private final Lock lock = new ReentrantLock();
    private int count = 0;



    public void inc() {
        lock.lock();
        try {
            count = count + 1;
        } finally {
            lock.unlock();
        }
    }

    public int getCount() {
        return count;
    }
}
