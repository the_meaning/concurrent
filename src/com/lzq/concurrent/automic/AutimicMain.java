package com.lzq.concurrent.automic;

import java.util.concurrent.atomic.AtomicInteger;

public class AutimicMain {

    public static void main(String[] args) throws InterruptedException {
        // 偏移量


        AtomicInteger atoInt = new AtomicInteger();

        for (int i = 0; i < 1000; i++) {
            new Thread(atoInt::incrementAndGet).start();
        }

        Thread.sleep(3000);
        System.out.println(atoInt.get());
    }


}
