package com.lzq.concurrent.waitnotify.producercomsumer;

import java.util.Random;

public class Consumer extends Thread {

    private MyQueue queue;
    private Random random = new Random();

    public Consumer(MyQueue queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        while (true) {
            String element = queue.get();
            System.out.println("\t\t消费的数据: " + element + ", 当前队列所剩数量：" + queue.getSize());
            try {
                sleep(random.nextInt(300));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
