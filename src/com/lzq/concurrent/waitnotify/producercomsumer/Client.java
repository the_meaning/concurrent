package com.lzq.concurrent.waitnotify.producercomsumer;

/**
 * 注意： 这里只有一个生产者和消费者，如果有多个生产者和消费者线程呢？
 */

public class Client {
    public static void main(String[] args) throws InterruptedException {
        MyQueue myQueue = new MyQueue();
        new Consumer(myQueue).start();
        new Producer(myQueue).start();
        Thread.sleep(5000);
        System.exit(0);
    }

}
