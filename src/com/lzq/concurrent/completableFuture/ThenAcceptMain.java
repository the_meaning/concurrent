package com.lzq.concurrent.completableFuture;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

// thenAccept 可以传递中间参数 把第一个任务的结果传给第二个任务当参数
// thenAccept 的参数为 Consumer
public class ThenAcceptMain {
    public static void main(String[] args) throws ExecutionException,
            InterruptedException {
        CompletableFuture<Void> future = CompletableFuture.supplyAsync(() -> {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("返回中间结果");
            return "这是中间结果";
        }).thenAccept((param) -> {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("任务执行后获得前面的中间结果：" + param);
        });

        // 阻塞等待任务执行完成
        future.get();
        System.out.println("任务执行完成");
    }
}