package com.lzq.concurrent.completableFuture;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

// 异步执行，不需要返回结果的 , 不用自己写 future.complete() 表示完成
public class RunAsyncMain {

    public static void main(String[] args) throws ExecutionException, InterruptedException {

        CompletableFuture future = CompletableFuture.runAsync(new Runnable() {
            @Override
            public void run() {
                System.out.println("启动线程执行任务");
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("任务执行完毕");
            }
        });

        // 依旧阻塞
        Object result = future.get();
        System.out.println("主线程===");
        System.out.println(result); // 获取结果也是为 null

    }

}
