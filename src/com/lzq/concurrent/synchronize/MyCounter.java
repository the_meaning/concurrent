package com.lzq.concurrent.synchronize;

public class MyCounter {
    private int count = 0;

    public void inc() {
        count = count + 1;
    }

    public int getCount() {
        return count;
    }
}
