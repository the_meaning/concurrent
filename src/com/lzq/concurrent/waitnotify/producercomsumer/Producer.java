package com.lzq.concurrent.waitnotify.producercomsumer;

import java.util.Random;

public class Producer extends Thread {

    private MyQueue queue;
    private Random random = new Random();

    public Producer(MyQueue queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        int index = 0;
        while (true) {
            String element = "data " + index;
            queue.put(element);
            System.out.println("生产了数据：" + element + ", 当前队列还剩个数：" + queue.getSize());
            try {
                sleep(random.nextInt(300));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            index++;
        }
    }
}
