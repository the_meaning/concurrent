package com.lzq.concurrent.synchronize;

public class MyCounter1 {
    private int count = 0;

    public synchronized void inc() {
        count = count + 1;
    }

    public int getCount() {
        return count;
    }
}
