package com.lzq.lambda;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * 从 Lambda2 和 Lambda3 中可以看出, 每次我们要使用 lambda 表达式简化的时候, 都需要自己定义一个接口进行实现,
 *      其实 Java 内部就已经提供了多种函数式接口, 不用我们自己写
 *
 *
 * 内置四大核心接口
 *      Consumer<T> : 消费型接口
 *          void accept(T t);
 *      Supplier<T> : 供给型接口
 *          T get();
 *      Function<T, R> : 函数型接口
 *          R apply(T t)
 *      Predicate<T> : 断言型接口
 *          boolean test(T t)
 */
public class Lambda4 {

    // consumer
    public static void testConsumer(double money, Consumer<Double> consumer) {
        consumer.accept(money);
    }

    // Supplier
    public static List<Integer> testSupplier(int num, Supplier<Integer> supplier) {
        List<Integer> resultList = new ArrayList<>();
        for (int i = 0; i < num; i++) {
            resultList.add(supplier.get());
        }
        return resultList;
    }


    public static void main(String[] args) {
        // 1
        testConsumer(10000, m -> {
            System.out.println("执行任务-- 消费: " + m);
        });

        // 2
        List<Integer> integers = testSupplier(10, () -> new Random().nextInt(100)); // 无参数,因此是 () ->
        System.out.println(integers);
    }

}
