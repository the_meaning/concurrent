package com.lzq.lambda;

/**
 * 按要求写个代码:
 *      1 声明函数式接口, 接口中声明抽象办法, public String getValue(String str);
 *      2 声明类 Lambda2, 类中编写办法使用接口作为参数, 将一个字符串转换成大写, 并做为办法的返回值
 *      3 再将一个字符串的 2~4 个位置进行截取
 */

public class Lambda2 {
    // 处理字符串的办法
    public static String strHandle(String str, MyFunction myFunction) {
        return myFunction.getValue(str); // 对字符串使用接口中的办法进行处理
    }

    public static void main(String[] args) {
        // 需求2
        String result1 = strHandle(" /t/t 2023年03月18日  ", str -> str.trim());
        System.out.println(result1);

        // 需求3
        String result2 = strHandle("2023年03月18日", str -> str.substring(2, 4));
        System.out.println(result2);
    }
}

// @FunctionalInterface
interface MyFunction { // 特征: 1接口 2里面有办法 3一个参数 4一个返回值 5String类型
    public String getValue(String str);
}