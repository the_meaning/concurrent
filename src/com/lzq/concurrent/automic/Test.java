package com.lzq.concurrent.automic;

public class Test {
    public static void main(String[] args) throws InterruptedException {
        Integer integer = 0;

        new IntegerThread(integer).start();

        Thread.sleep(10);
        System.out.println(integer); // 注意一下，这里仍然是 0，因为 Integer++ 相当于重新 new 了一个 Integer？
    }

}
