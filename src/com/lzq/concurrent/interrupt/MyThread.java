package com.lzq.concurrent.interrupt;

public class MyThread extends Thread {
    @Override
    public void run() {
        try {
            sleep(1000);
        } catch (InterruptedException e) {
            System.out.println("线程被 interrupt 唤醒后继续执行");
            e.printStackTrace();
        }
    }
}
