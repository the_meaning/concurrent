package com.lzq.concurrent.completableFuture;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class CompletableFutureMain {

    public static void main(String[] args) throws ExecutionException, InterruptedException {

        CompletableFuture<String> future = new CompletableFuture<>();

        new Thread() {
            @Override
            public void run() {
                System.out.println("启动线程执行任务");
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                String result = "execute result";
                future.complete(result);
            }
        }.start();

        System.out.println("主线程已提交任务");
        // 该办法是阻塞办法
        String result = future.get();
        System.out.println(result);
    }

}
