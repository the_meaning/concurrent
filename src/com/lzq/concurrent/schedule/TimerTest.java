package com.lzq.concurrent.schedule;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Timer;
import java.util.TimerTask;

/**
 * 使用 Timer 和 TimerTask 来执行周期性任务
 */
public class TimerTest {

    public static void main(String[] args) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String time = dateTimeFormatter.format(LocalDateTime.now());
        System.out.println("当前时间: " + time);

        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                String time = dateTimeFormatter.format(LocalDateTime.now());
                System.out.println("周期任务执行 " + time);
            }
        }, 3000, 2000);

    }

}
