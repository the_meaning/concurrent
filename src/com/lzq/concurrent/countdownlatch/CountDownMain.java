package com.lzq.concurrent.countdownlatch;

import java.util.concurrent.CountDownLatch;

/**
 * CountDownLatch 作用： 可以使主线程等待 指定数量的 worker 线程运行完毕后再结束，
 *      主线程先 latch.await(); 阻塞，等到 其他线程 latch.countDown(); 把数量减少到 0 才会放行。
 *
 */
public class CountDownMain {

    public static void main(String[] args) throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(5);

        for (int i = 0; i < 5; i++) {
            new MyThread("线程-" + (i + 1), latch).start();
        }

        latch.await();
        System.out.println("主线程运行结束 - " + Thread.currentThread().getName());

    }

}
