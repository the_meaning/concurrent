package com.lzq.concurrent.futurepattern;

public interface Data {
    String getContent();
}