package com.lzq.concurrent.futurepattern;

public class Host {
    public   Data request(final int count, final char c) {
        System.out.println("\trequest(" + count + ", " + c + ") 开始");

        // 创建FutureData对象
        final   FutureData future = new   FutureData();
        // 启动新线程，创建RealData对象
        new Thread() {
            @Override
            public void run() {
                  RealData realData = new   RealData(count, c);
                future.setRealData(realData);
            }
        }.start();
        System.out.println("\trequest(" + count + ", " + c + ") 结束");
        // 返回提货单
        return future;
    }
}