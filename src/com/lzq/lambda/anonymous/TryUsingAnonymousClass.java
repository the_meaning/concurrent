package com.lzq.lambda.anonymous;

/**
 * 内部构造类的变量为什么要用 final 修饰?
 *      如下, 一旦外部调用办法 useMyInterface() 执行完毕,其实 局部变量 numbber 的生命周期就结束了,
 *      不存在了,但是内存中还存在一个 类(内部类) 去引用这个变量, 可变量已经不在了, 会有问题,
 *      因此 Java 在局部内部类里面存的实际上是 局部变量 的复制品, 也就 要求用 final 进行修饰, 不让更改, 不然导致复制品与原品不一样.
 */

public class TryUsingAnonymousClass {

    public void useMyInterface() {
        final Integer number = 123;
        System.out.println(number);

        MyInterface myInterface = new MyInterface() {
            @Override
            public void doSomething() {
                System.out.println(number);
            }
        };

        myInterface.doSomething();
        System.out.println(number);
    }
}

// 上面这个类反编译后会变成:
// 内部构造类的变量为什么要用 final 修饰:  https://blog.csdn.net/u011617742/article/details/51613519
class TryUsingAnonymousClass$1 implements MyInterface {
    private final TryUsingAnonymousClass this$0;
    private final Integer paramInteger;

    TryUsingAnonymousClass$1(TryUsingAnonymousClass this$0, Integer paramInteger) {
        this.this$0 = this$0;
        this.paramInteger = paramInteger;
    }

    public void doSomething() {
        System.out.println(this.paramInteger);
    }
}