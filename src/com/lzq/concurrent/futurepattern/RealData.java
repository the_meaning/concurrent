package com.lzq.concurrent.futurepattern;

public class RealData implements Data {

    private final String content;

    public RealData(int count, char c) {
        System.out.println("\t组装RealData(" + count + ", " + c + ") 开始");
        char[] buffer = new char[count];
        for (int i = 0; i < count; i++) {
            buffer[i] = c;
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("\t\t组装RealData(" + count + ", " + c + ") 结束");
        this.content = new String(buffer);
    }

    @Override
    public String getContent() {
        return content;
    }
}