package com.lzq.concurrent.synchronize;

public class Main {

    public static void main(String[] args) throws InterruptedException {

        MyCounter counter = new MyCounter();

        for (int i = 0; i < 5000; i++) {
            new Thread(counter::inc).start();
        }
        Thread.sleep(1000);
        System.out.println("最终结果：" + counter.getCount());

        MyCounter1 synchronizedCounter = new MyCounter1();
        for (int i = 0; i < 5000; i++) {
            new Thread(synchronizedCounter::inc).start();
        }
        Thread.sleep(1000);
        System.out.println("最终结果：" + synchronizedCounter.getCount());

        MyCounter2 lockCounter = new MyCounter2();
        for (int i = 0; i < 5000; i++) {
            new Thread(lockCounter::inc).start();
        }
        Thread.sleep(1000);
        System.out.println("最终结果：" + lockCounter.getCount());

    }

}
