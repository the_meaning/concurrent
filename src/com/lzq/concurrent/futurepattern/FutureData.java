package com.lzq.concurrent.futurepattern;

public class FutureData implements Data {

    private RealData realData = null;
    private boolean ready = false;

    public synchronized void setRealData(RealData realData) {
        // balking，如果已经准备好，就返回
        if (ready) {
            return;
        }
        this.realData = realData;
        this.ready = true;
        notifyAll();
    }

    @Override
    public synchronized String getContent() {
        // guarded suspension
        while (!ready) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return realData.getContent();
    }
}