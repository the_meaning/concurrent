package com.lzq.concurrent.threadpool;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ThreadPoolMain {

    private static ThreadPoolExecutor poolExecutor = new ThreadPoolExecutor(
            3,
            5,
            5,
            TimeUnit.SECONDS,
            new LinkedBlockingQueue<>(3)
    ) {
        @Override
        protected void beforeExecute(Thread t, Runnable r) {
            // 钩子函数
            System.out.println("钩子函数---执行之前");
        }

        @Override
        protected void afterExecute(Runnable r, Throwable t) {
            // 钩子函数
            System.out.println("钩子函数---执行之后");
        }
    };

    public static void main(String[] args) {

        poolExecutor.execute(new MyThread());
        System.out.println("主线程执行--");

        poolExecutor.shutdown();
    }

}

