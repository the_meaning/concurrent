package com.lzq.concurrent.waitnotify.producercomsumer;

/**
 * 实现一个生产者消费者队列
 *      依靠 wait 和 notify 进行阻塞唤醒
 */

public class MyQueue {

    // 数据队列
    private String[] data = new String[10];
    // 下一个要存储的位置
    private int putIndex = 0;
    // 下一个要获取的位置
    private int getIndex = 0;
    // 队列当前元素个数
    private int size = 0;

    // 生产数据
    public synchronized void put(String element) {
        if (size == data.length) { // 满了就阻塞，等待唤醒
            try {
                System.out.println("阻塞生产进程");
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        data[putIndex] = element;
        putIndex++;
        size++;
        if (putIndex == data.length) {
            putIndex = 0;
        }
        notify(); // 存放了一个就唤醒消费者可以消费了
    }


    // 消费数据
    public synchronized String get() {
        if (size == 0) { // 队列为空就阻塞
            try {
                System.out.println("阻塞消费进程");
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        String result = data[getIndex];
        getIndex++;
        size--;
        if (getIndex == data.length) {
            getIndex = 0;
        }
        notify(); // 消费了一个就唤醒生产者
        return result;
    }

    public int getSize() {
        return size;
    }
}
