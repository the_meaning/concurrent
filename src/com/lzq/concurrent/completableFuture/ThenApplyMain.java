package com.lzq.concurrent.completableFuture;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.Function;

// thenApply() 对比 thenAccept() 就是第二个任务有了返回值
// thenApply() 的参数是一个 Function 函数式接口
public class ThenApplyMain {
    public static void main(String[] args) throws ExecutionException,
            InterruptedException {
        CompletableFuture<Integer> future = CompletableFuture.supplyAsync(() -> {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("返回中间结果");
            return "abcdefg";
        }).thenApply(new Function<String, Integer>() {
            @Override
            public Integer apply(String middle) {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("获取中间结果，再次计算返回");
                return middle.length();
            }
        });
        Integer integer = future.get();
        System.out.println("最终的结果为：" + integer);
    }
}


