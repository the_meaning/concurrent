package com.lzq.concurrent.blockingqueue;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class QueueMain {

    public static void main(String[] args) throws InterruptedException {
        // ArrayBlockingQueue 有三种添加办法，可进去看源码
        ArrayBlockingQueue<String> queue = new ArrayBlockingQueue<>(3);
        queue.put("1"); // 满了会阻塞，此时和 wait()、sleep() 状态类似，可抛出 InterruptedException 异常
        queue.add("1"); // 满了抛出 IllegalStateException("Queue full"); 异常  -- 底层也是调用 offer() 办法
        queue.offer("1"); // 满了返回 false


        LinkedBlockingQueue<String> linkedQueue = new LinkedBlockingQueue<>();
        linkedQueue.take();
        linkedQueue.put("");






        System.out.println("======");
        QueueMain main = new QueueMain();

        try {
            main.method1();
        } catch (IllegalStateException e) {
            System.out.println("捕获到了 method1 的异常");
        }

        main.method2();

        try {
            main.method1();
        } catch (IllegalStateException e) {
            e.printStackTrace();
            System.out.println("捕获到了 method1 的异常");
        }


    }

    private void method1() throws IllegalStateException{
        throw new IllegalStateException("异常");
    }

    private void method2() {
        TestException test = new TestException();
        test.method1();
    }


}
