package com.lzq.lambda;

/**
 * 1 声明一个带两个泛型的函数式接口, 泛型类型为<T, R>
 *     T 为参数, R 为返回值
 * 2 接口中声明对应的抽象办法
 * 3 在 Lambda3 类中声明办法, 使用接口作为参数, 计算两个 long 型参数的和.
 * 4 再计算两个 long 型参数的乘积
 */
public class Lambda3 {

    // 对于两个 long 型数据进行处理
    public static long handleMethod(long param1, long param2, MyFunction2<Long, Long> myFunction2) {
        return myFunction2.getValue(param1, param2);
    }

    public static void main(String[] args) {
        // 需求第 3 点
        long result1 = handleMethod(100L, 200L, (p1, p2) -> p1 + p2);
        System.out.println(result1);

        // 需求第 4 点
        long result2 = handleMethod(100L, 200L, (p1, p2) -> p1 * p2);
        System.out.println(result2);
    }

}

interface MyFunction2<T, R> {
    R getValue(T t1, T t2);
}